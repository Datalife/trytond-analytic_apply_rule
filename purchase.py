# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .account import ApplyRuleMixin


class Rule(metaclass=PoolMeta):
    __name__ = 'analytic_account.rule'

    purchase = fields.Boolean('Purchase', select=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        for fieldname in ('journal', 'account'):
            field = getattr(cls, fieldname)
            if 'readonly' in field.states:
                field.states['readonly'] |= Eval('purchase')
            else:
                field.states['readonly'] = Eval('purchase')
            field.depends.append('purchase')

    @fields.depends('purchase')
    def on_change_purchase(self):
        if self.purchase:
            self.journal = None
            self.account = None

    @fields.depends('purchase')
    def on_change_with_party_visible(self, name=None):
        return super().on_change_with_party_visible(name=name) or self.purchase


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @property
    def rule_pattern(self):
        pattern = super().rule_pattern
        pattern['purchase'] = False
        return pattern


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @fields.depends('party', 'lines')
    def on_change_party(self):
        super().on_change_party()
        if self.party:
            for line in self.lines:
                line.apply_analytic_rule()


class PurchaseLine(ApplyRuleMixin, metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('purchase', 'analytic_accounts',
            methods=['apply_analytic_rule'])
    def on_change_purchase(self):
        try:
            super().on_change_purchase()
        except AttributeError:
            pass
        if self.purchase and self.analytic_accounts:
            self.apply_analytic_rule()

    @fields.depends('purchase')
    def _get_analytic_rule_template(self):
        return self.purchase

    def rule_analytic_pattern(self, template):
        pattern = super().rule_analytic_pattern(template)
        pattern['purchase'] = True
        return pattern
