# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .account import ApplyRuleMixin


class Rule(metaclass=PoolMeta):
    __name__ = 'analytic_account.rule'

    sale = fields.Boolean('Sale', select=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        for fieldname in ('journal', 'account'):
            field = getattr(cls, fieldname)
            if 'readonly' in field.states:
                field.states['readonly'] |= Eval('sale')
            else:
                field.states['readonly'] = Eval('sale')
            field.depends.append('sale')

    @fields.depends('sale')
    def on_change_sale(self):
        if self.sale:
            self.journal = None
            self.account = None

    @fields.depends('sale')
    def on_change_with_party_visible(self, name=None):
        return super().on_change_with_party_visible(name=name) or self.sale


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @property
    def rule_pattern(self):
        pattern = super().rule_pattern
        pattern['sale'] = False
        return pattern


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @fields.depends('party', 'lines')
    def on_change_party(self):
        super().on_change_party()
        if self.party:
            for line in self.lines:
                line.apply_analytic_rule()


class SaleLine(ApplyRuleMixin, metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('sale', 'analytic_accounts',
            methods=['apply_analytic_rule'])
    def on_change_sale(self):
        try:
            super().on_change_sale()
        except AttributeError:
            pass
        if self.sale and self.analytic_accounts:
            self.apply_analytic_rule()

    @fields.depends('sale', '_parent_sale.company', '_parent_sale.party')
    def _get_analytic_rule_template(self):
        return self.sale

    def rule_analytic_pattern(self, template):
        pattern = super().rule_analytic_pattern(template)
        pattern['sale'] = True
        return pattern
