datalife_analytic_apply_rule
============================

The analytic_apply_rule module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-analytic_apply_rule/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-analytic_apply_rule)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
