# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Or

from .account import ApplyRuleMixin


class Rule(metaclass=PoolMeta):
    __name__ = 'analytic_account.rule'

    invoice_in = fields.Boolean('Supplier Invoice', select=True)
    invoice_out = fields.Boolean('Customer Invoice', select=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        for fieldname in ('journal', 'account'):
            field = getattr(cls, fieldname)
            if 'readonly' in field.states:
                field.states['readonly'] |= Or(
                    Eval('invoice_in'), Eval('invoice_out'))
            else:
                field.states['readonly'] = Or(
                    Eval('invoice_in'), Eval('invoice_out'))
            field.depends.extend(['invoice_in', 'invoice_out'])

    @fields.depends('invoice_in')
    def on_change_invoice_in(self):
        if self.invoice_in:
            self.journal = None
            self.account = None

    @fields.depends('invoice_out')
    def on_change_invoice_out(self):
        if self.invoice_out:
            self.journal = None
            self.account = None

    @fields.depends('invoice_in', 'invoice_out')
    def on_change_with_party_visible(self, name=None):
        return (super().on_change_with_party_visible(name=name)
            or self.invoice_in or self.invoice_out)


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    @property
    def rule_pattern(self):
        pattern = super().rule_pattern
        pattern['invoice_in'] = False
        pattern['invoice_out'] = False
        return pattern


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @fields.depends('party', 'lines')
    def on_change_party(self):
        super().on_change_party()
        if self.party:
            for line in self.lines:
                line.apply_analytic_rule()


class InvoiceLine(ApplyRuleMixin, metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('invoice', 'analytic_accounts',
            methods=['apply_analytic_rule'])
    def on_change_invoice(self):
        try:
            super().on_change_invoice()
        except AttributeError:
            pass
        if self.invoice and self.analytic_accounts:
            self.apply_analytic_rule()

    @fields.depends('invoice', '_parent_invoice.party')
    def _get_analytic_rule_template(self):
        return self.invoice

    def rule_analytic_pattern(self, template):
        pattern = super().rule_analytic_pattern(template)
        pattern['invoice_%s' % self.invoice.type] = True
        return pattern
